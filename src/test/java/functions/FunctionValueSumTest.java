package functions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionValueSumTest {

    @Test
    public void functionValSrtaightLine() throws FunctionalException {
        assertEquals(12,new FunctionValueSum<StraightLine>().solution
                (new StraightLine(1,3,1,2)),0.0001);
    }
    @Test
    public void functionValExp() throws FunctionalException {
        assertEquals(36.1928,new FunctionValueSum<Exponent>().solution
                (new Exponent(1,3,1,2)),0.0001);
    }
    @Test
    public void functionValSin() throws FunctionalException {
        assertEquals(Math.sqrt(2) + 2,new FunctionValueSum<Sinus>().solution
                (new Sinus(0,Math.PI/2,2,1)),0.0001);
    }
}
package functions;

public interface IFunctions {
    double getFunctionValue(final double x)throws FunctionException;
    double getA();
    double getB();
}

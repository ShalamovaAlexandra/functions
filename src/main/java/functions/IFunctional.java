package functions;

import functions.IFunctions;
import functions.RationalFunction;

public interface IFunctional <T extends IFunctions>{
    double solution(T function)throws FunctionalException;
}

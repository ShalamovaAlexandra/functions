package functions;

public class FunctionValueSum <T extends IFunctions> implements IFunctional<T>{
    public double solution(T function) throws FunctionalException {
        try {
            return function.getFunctionValue(function.getA()) +
                    function.getFunctionValue(function.getB()) +
                    function.getFunctionValue((function.getA() + function.getB()) / 2);
        } catch (FunctionException e){
            throw new FunctionalException(e);
        }
    }
}
